# How to run it

1. Create a user in your MySQL database, and configure it using the environment variable DATABASE\_URI:

```console
$ export DATABASE_URI='mysql+mysqldb://<USER>:<PASSWORD>@<HOST>[:<PORT>]/<DATABASE>?charset=utf8mb4'
$ # Example: "mysql+mysqldb://scott:tiger@localhost/mydatabase?charset=utf8mb4"
```

You can use the SQL script [`create_db_user.sql`](create_db_user.sql) to create a user and a database:

```console
$ mysql -u root -p < create_db_user.sql
```

3. Clone this repository to your local filesystem (or download it and extract it), and cd to it:

```console
$ git clone 'https://gitlab.com/wmb/flask-mail-app.git'
$ cd flask-mail-app
```

2. Install the necessary Python packages in a virtual environment:

```console
$ python3 -m venv venv
$ . ./venv/bin/activate
(venv) $ pip install -r requirements.txt
...
(venv) $
```

3. Initialize the database

```console
(venv) $ flask db upgrade
```

4. Create the machine learning models

```console
(venv) $ python create_models.py
```

If the script above fails then you need to increase the max allowed packet size for MySQL. This can be done by adding
a `max_allowed_packet.cnf` file to /etc/my.cnf.d containing the following

```ini
[mysqld]
max_allowed_packet=256M
```

and re-run the script. Your database should now be populated with the machine learning models used by the application.

5. Start the server

```console
(venv) $ FLASK_ENV=development FLASK_APP=app flask run
```

6. Open your web browser and navigate to http://localhost:5000/
