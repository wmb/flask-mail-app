(function () {
    "use strict";

    async function composeMessage(fd, url) {
        const statusp = document.getElementById("message-sending-status");
        const progressp = document.getElementById("message-sending-progress");
        const methods_ul = document.getElementById("methods-tested");
        const init = ((fd) => {
            const o = Object.create(null);
            o.method = "POST";
            o.body = fd;
            o.mode = "same-origin";
            o.credentials = "same-origin";
            o.keepalive = true;
            return o;
        })(fd);
        const decoder = new TextDecoder();
        const resp = await fetch(url, init);
        const reader = resp.body.getReader();

        const otherMessageHandler = async (message, { done, value }) => {
            if (!done) {
                const reply = (() => {
                    try {
                        const j = JSON.parse(decoder.decode(value));
                        return j;
                    } catch (e) {
                        console.error(decoder.decode(value));
                        return null;
                    }
                })();
                console.log(reply);

                if (reply.done === true) {
                    return reader
                        .read()
                        .then(
                            otherMessageHandler.bind(null, reply.message_id)
                        )
                        .catch(console.error);
                }

                methods_ul.appendChild(
                    (function (m) {
                        const li = document.createElement("li");
                        li.innerHTML = `${m} &check;`;
                        return li;
                    })(reply.method)
                );

                const progressPercent =
                    ((parseInt(reply.progress) + 1) /
                        parseInt(progressp.getAttribute("aria-valuemax"))) *
                    100;
                progressp.textContent = `${Math.round(progressPercent)} %`;
                progressp.setAttribute("aria-valuenow", reply.progress + 1);
                progressp.style.width = `${progressPercent}%`;

                const is_last =
                    parseInt(progressp.getAttribute("aria-valuenow")) ===
                    parseInt(progressp.getAttribute("aria-valuemax"));

                if (is_last) {
                    statusp.textContent = "Adding mail to the database...";
                    progressp.classList.remove(
                        ...["striped", "animated"].map(
                            (x) => "progress-bar-" + x
                        )
                    );
                    progressp.classList.add("bg-success");
                }

                return reader
                    .read()
                    .then(otherMessageHandler.bind(null, message))
                    .catch(console.error);
            }

            console.log("DONE!");
            statusp.textContent = `Done. Redirecting you to /message/${message}...`;
            return message;
        };

        const firstMessageHandler = async ({ done, value }) => {
            if (!done) {
                const reply = (() => {
                    try {
                        const j = JSON.parse(decoder.decode(value));
                        return j;
                    } catch (e) {
                        console.error(decoder.decode(value));
                        return null;
                    }
                })();

                if (!reply) {
                    statusp.textContent = "Failed.";
                    return null;
                }

                console.log(reply);

                statusp.textContent = "Checking email for spam...";

                progressp.textContent = "0 %";
                progressp.setAttribute("aria-valuemax", reply.methods);
                progressp.style.width = "0";
                progressp.parentElement.classList.remove("d-none");

                methods_ul.classList.remove("d-none");

                return reader
                    .read()
                    .then(otherMessageHandler.bind(null, null))
                    .catch(console.error);
            }
            console.error("Done before I received anything!");
            return null;
        };

        statusp.textContent = "Waiting for a reply from the server";
        statusp.parentElement.classList.remove("d-none");

        return reader.read().then(firstMessageHandler).catch(console.error);
    }

    function formSubmitHandler(event) {
        const form = event.target;
        event.preventDefault();
        composeMessage(new FormData(form), form.action)
            .then((message) => {
                console.log("THEN'd message:", message);
                if (message !== null && message !== undefined) {
                    setTimeout(() => {
                        document.location = `/message/${message}`;
                    }, 100);
                } else {
                    setTimeout(() => {
                        document.location.reload();
                    }, 1000);
                    confirm("Check your inputs and try again.");
                }
            })
            .catch(console.error);
    }

    window.addEventListener("load", function (event) {
        document.forms[0].addEventListener("submit", formSubmitHandler);
    });
})();

/* vim: set ts=8 sw=4 sts=4 et: */
