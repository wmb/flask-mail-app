function startStream(event) {
	console.debug("startStream");
	const statusp = document.getElementById("status");
	const ul = document.getElementById("chunks");
	const fetching = fetch("/stream");
	const decoder = new TextDecoder();
	fetching.then(function (resp) {
		const reader = resp.body.getReader();
		reader.read()
			.then(function handleRead({ done, value }) {
				if (done) {
					statusp.textContent = "Done.";
					return;
				}
				const li = document.createElement("li");
				li.textContent = decoder.decode(value);
				ul.appendChild(li);
				return reader
					.read()
					.then(handleRead)
					.catch(console.error);
			})
			.catch(console.error);
	}).catch(console.error);
	statusp.textContent = "Waiting for reply";
	event.preventDefault();
}

window.addEventListener("load", function (e) {
	console.log("Log");
	document.getElementById("streamBtn").addEventListener(
		"click",
		startStream
	);
});
