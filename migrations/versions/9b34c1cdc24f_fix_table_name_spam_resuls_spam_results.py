"""Fix table name (spam_resuls -> spam_results)

Revision ID: 9b34c1cdc24f
Revises: c7d0fe91fd51
Create Date: 2021-08-12 22:06:30.623439

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '9b34c1cdc24f'
down_revision = 'c7d0fe91fd51'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('spam_resuls', 'spam_results')


def downgrade():
    op.rename_table('spam_results', 'spam_resuls')
