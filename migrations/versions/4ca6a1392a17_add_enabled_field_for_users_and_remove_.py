""" - Add enabled field for users
    - Remove models from methods, only store a path
    - Add probability field for spam_results (because the DL model returns a
      float and we should keep it alongside the boolean.

Revision ID: 4ca6a1392a17
Revises: cbd14524c805
Create Date: 2021-08-23 03:23:23.144230

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = "4ca6a1392a17"
down_revision = "cbd14524c805"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "spam_results", sa.Column("probability", sa.Float(), nullable=True)
    )
    op.add_column(
        "users", sa.Column("enabled", sa.Boolean(True), nullable=False)
    )
    op.alter_column(
        "spam_methods",
        "model",
        comment="Path to the model",
        type_=sa.String(255),
        existing_nullable=False,
    )


def downgrade():
    op.drop_column("users", "enabled")
    op.drop_column("spam_results", "probability")
    op.alter_column(
        "spam_methods",
        "model",
        comment=False,
        type_=mysql.LONGBLOB(length=33554431),
        existing_nullable=False,
    )
