-- {{{ Define the variables

SET @username = 'spamuser1';
SET @password = '4T[DWO8rqzp(26Z@';
SET @db_name  = 'spamuser1';

-- }}}

SET autocommit = 0;
START TRANSACTION;

-- {{{ Escape user and db name

SET @username_full = CONCAT_WS('@',
	QUOTE(@username),
	QUOTE('%')
);
SET @db_full = CONCAT_WS(@db_name, '`', '`');

-- }}}

-- {{{ Drop existing user and db

PREPARE du FROM CONCAT('DROP USER IF EXISTS ', @username_full);
EXECUTE du;
DEALLOCATE PREPARE du;

PREPARE dd FROM CONCAT('DROP DATABASE IF EXISTS ', @db_full);
EXECUTE dd;
DEALLOCATE PREPARE dd;

-- }}}

-- {{{ Create the user

SET @create_user_query = CONCAT_WS(' ',
	'CREATE USER',
		@username_full,
	'IDENTIFIED',
		'BY', QUOTE(@password)
);

SELECT @create_user_query as '* Create user:' FROM DUAL;
PREPARE create_user_stmt FROM @create_user_query;
EXECUTE create_user_stmt;
DEALLOCATE PREPARE create_user_stmt;

-- }}}

-- {{{ Give them permissions

SET @give_perms_query = CONCAT_WS(' ',
	'GRANT',
		'USAGE',
	'ON',
		'*.*',
	'TO',
		@username_full,
	'REQUIRE',
		'NONE',
	'WITH',
		'MAX_QUERIES_PER_HOUR 0',
		'MAX_CONNECTIONS_PER_HOUR 0',
		'MAX_UPDATES_PER_HOUR 0',
		'MAX_USER_CONNECTIONS 0'
);

SELECT @give_perms_query AS '* Give them permissions:' FROM DUAL;
PREPARE give_perms_stmt FROM @give_perms_query;
EXECUTE give_perms_stmt;
DEALLOCATE PREPARE give_perms_stmt;

-- }}}

-- {{{ Create a database with the same name

SET @create_db_query = CONCAT_WS(' ',
	'CREATE DATABASE IF NOT EXISTS',
		@db_full
);

SELECT @create_db_query AS '* Create database:' FROM DUAL;
PREPARE create_db_stmt FROM @create_db_query;
EXECUTE create_db_stmt;
DEALLOCATE PREPARE create_db_stmt;

-- }}}

-- {{{ Give the user all permissions on this database

SET @give_db_perms_query = CONCAT_WS(' ',
	'GRANT',
		'ALL PRIVILEGES',
	'ON',
		CONCAT(@db_full, '.*'),
	'TO',
		@username_full
);

SELECT @give_db_perms_query AS '* Give the user permissions on the database' FROM DUAL;
PREPARE give_db_perms_stmt FROM @give_db_perms_query;
EXECUTE give_db_perms_stmt;
DEALLOCATE PREPARE give_db_perms_stmt;

-- }}}

COMMIT;

-- vim: set foldmethod=marker:
