#!/usr/bin/env python
# coding: utf-8

import gc
import pathlib
import time

from sklearn import (
    # tree,
    ensemble,
    feature_extraction,
    linear_model,
    metrics,
    naive_bayes,
    neighbors,
    svm,
)
from tensorflow.keras.preprocessing.sequence import pad_sequences
import joblib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf

from spam_model import SpamModel, ModelType

import app


class Config:
    vectorizer_save_path: pathlib.Path = pathlib.Path(
        "vectorizers", "tdf-if.vec"
    )
    ml_models_save_dir: pathlib.Path = pathlib.Path("models", "ml")
    saved_dl_path: pathlib.Path = pathlib.Path("saved-dl-model")
    dl_models_save_dir: pathlib.Path = pathlib.Path("models", "dl")
    tokenizer_save_path: pathlib.Path = pathlib.Path(
        "vectorizers", "dl-tokenizer"
    )


sklearn_metrics = ("accuracy", "precision", "recall", "f1", "roc_auc")


def pred_proba_to_class(pred_proba):
    series = pd.Series(np.reshape(pred_proba, (pred_proba.shape[0],))).map(
        lambda p: int(p >= 0.5)
    )
    return np.asarray(series, dtype=np.uint8)


# The Deep Learning model is being tested against data that it hasn't been
# trained on, unlike the other ML models, because my PC is too slow to run the
# training here. I should have calculated the metrics and saved them from
# Google Colab instead of doing them here again :(
def load_saved_dl_model(
    x_test, y_test, stats: pd.DataFrame
) -> app.SpamMethod:
    print(
        "Loading saved Deep Learning model from {}".format(
            Config.saved_dl_path
        )
    )
    name = "Deep Learning"
    info_path = Config.saved_dl_path / "info.xz"
    model_path = Config.saved_dl_path / "model"

    model = tf.keras.models.load_model(model_path.as_posix())
    info = joblib.load(info_path.as_posix())
    del info["data"]
    tokenizer = info.get("tokenizer")
    del info["tokenizer"]
    gc.collect()

    joblib.dump(tokenizer, Config.tokenizer_save_path, compress=True)

    saved_model = SpamModel(
        name, model_path, ModelType.DL, Config.tokenizer_save_path, info
    )
    compressed_model = saved_model.pickled()
    save_file = (
        (Config.dl_models_save_dir / name.replace(" ", "-").lower())
        .resolve()
        .relative_to(pathlib.Path(__file__).parent)
    )
    save_file.write_bytes(compressed_model)
    print("Wrote the model to the file {}".format(save_file))

    print("Tokenizing and padding sequences...")
    x_test = pad_sequences(
        tokenizer.texts_to_sequences(x_test), info.get("maxlen")
    )
    print("Done\n")

    gc.collect()

    print("Predicting with {} model...".format(name))
    y_pred = pred_proba_to_class(model.predict(x_test, batch_size=32))
    del x_test
    gc.collect()
    print("Done\n")

    db_metric_args: dict[str, float] = dict()

    print("Calculating metrics...")
    for metric in sklearn_metrics:
        metric_func = getattr(metrics, metric + "_score")
        db_metric_args[metric] = stats[metric][name] = metric_func(
            y_test, y_pred
        )
        print(" -> {}_score".format(metric))
    del y_test, y_pred
    gc.collect()
    print("Done\n")

    db_entry = app.SpamMethod(
        name=name, model=save_file.as_posix(), **db_metric_args
    )

    return db_entry


if __name__ == "__main__":

    # Create dirs
    print("Creating directories... ")
    dirs = {
        Config.ml_models_save_dir,
        Config.dl_models_save_dir,
        Config.vectorizer_save_path.parent,
        Config.tokenizer_save_path.parent,
    }
    for d in dirs:
        d.mkdir(mode=0o755, parents=True, exist_ok=True)
        print(" ->", d)
    print("Done\n")

    print("Loading data... ")

    data = joblib.load(Config.saved_dl_path / "info.xz").get("data")

    x_train = data["x_train"]
    y_train = data["y_train"]
    x_test = data["x_test"]
    y_test = data["y_test"]

    print("Done\n")

    # Uncomment the models you want to use
    models = {
        "Deep Learning": None,
        "K Nearest Neighbors": neighbors.KNeighborsClassifier(),
        # "Decision Tree":       tree.DecisionTreeClassifier(),
        "Random Forest": ensemble.RandomForestClassifier(),
        "Logistic Regression": linear_model.LogisticRegression(),
        "SGD Classifier": linear_model.SGDClassifier(
            max_iter=2000, n_jobs=-1
        ),
        "Naive Bayes": naive_bayes.MultinomialNB(),
        "SVM Linear": svm.SVC(kernel="linear"),
        "SVM Sigmoid": svm.SVC(kernel="sigmoid"),
        # "Nu SVC Linear": svm.NuSVC(kernel="linear"),
        # "Nu SVC Sigmoid": svm.NuSVC(kernel="sigmoid"),
    }

    stats = pd.DataFrame(index=models.keys(), columns=sklearn_metrics)

    app.db.session.add(load_saved_dl_model(x_test, y_test, stats))

    # TF-IDF vectorizer
    tfidf_vec = feature_extraction.text.TfidfVectorizer(
        strip_accents="unicode",
        ngram_range=(1, 4),
        use_idf=True,
        smooth_idf=True,
    )

    # Fit vectorizer
    print("Fitting TF-IDF vectorizer... ")
    texts = x_test.append(x_train, ignore_index=True)
    tfidf_vec.fit(texts)
    del texts
    gc.collect()
    print("Done\n")

    # Export (save) the vectorizer
    print(
        "Vectorizer saved at:",
        joblib.dump(tfidf_vec, Config.vectorizer_save_path, compress=True),
    )

    # Transform texts into a matrix using the vectorizer
    print("Transforming text with the TF-IDF vectorizer... ")
    x_train = tfidf_vec.transform(x_train)
    x_test = tfidf_vec.transform(x_test)
    print("Done\n")

    gc.collect()

    for name, model in models.items():

        if model is None:
            continue

        print("Training model", name)

        start_time = time.time()

        model.fit(x_train, y_train)

        training_duration = time.time() - start_time
        print("Done. Training took {} seconds".format(int(training_duration)))
        del start_time, training_duration

        saved_model = SpamModel(
            name, model, ModelType.ML, Config.vectorizer_save_path
        )

        compressed_model = saved_model.pickled()

        print(
            "Compressed pickled model size: {:.2f} MiB\n".format(
                len(compressed_model) / 0x100000
            )
        )

        save_file = (
            (Config.ml_models_save_dir / name.replace(" ", "-").lower())
            .resolve()
            .relative_to(pathlib.Path(__file__).parent)
        )
        save_file.write_bytes(compressed_model)
        print("Wrote the model to the file {}\n".format(save_file))

        y_pred = model.predict(x_test)

        db_metric_args: dict[str, float] = dict()

        for metric in sklearn_metrics:
            metric_func = getattr(metrics, metric + "_score")
            db_metric_args[metric] = stats[metric][name] = metric_func(
                y_test, y_pred
            )
        del y_pred
        gc.collect()

        # Save the models in the database
        db_entry = app.SpamMethod(
            name=name, model=save_file.as_posix(), **db_metric_args
        )

        app.db.session.add(db_entry)

    del x_train, y_train, x_test, y_test
    gc.collect()

    app.db.session.commit()
    app.db.session.close()

    print(78 * "-")

    # Show stats
    print(stats)

    # Show stats as a graph
    stats.transpose().plot.bar()
    plt.xlabel("Classifier")
    plt.ylabel("Percentage")
    plt.savefig("models_stats.png", dpi=500)

    stats.plot.bar()
    plt.xlabel("Percentage")
    plt.ylabel("Classifier")
    plt.savefig("models_stats2.png", dpi=500)
