import re
import string

import nltk


class PreprocessText:

    # Regex that matches numbers
    # numbers_regex = re.compile(r"^[\d+/~,.'-]+$")
    numbers_regex: re.Pattern = re.compile(r"\d+")

    # Simplified URL-matching regex
    urls_regex: re.Pattern = re.compile(
        r"(?:ht|f)tps?://(?:[\w-]+\.)*[\w-]+(?::\d+)?(?:/.*)?", re.I
    )

    # Simplified email-address-matching regex
    email_regex: re.Pattern = re.compile(r"[\w+#~|-]+@[\w-]+\.[a-z]+", re.I)

    def _get_stopwords(*langs: tuple[str]) -> set[str]:
        """This method is used to get the stopwords only"""
        nltk.download("stopwords", quiet=True)
        return set(nltk.corpus.stopwords.words(langs))

    def _get_punct_regex() -> re.Pattern:
        """This method is used to generate the punct_regex only"""
        punct = set(string.punctuation)
        punct.remove("'")
        punct = re.escape("".join(punct))
        return re.compile(r"[{}\s]+".format(punct), re.MULTILINE)

    # NLTK stopwords for the English and French languages
    stopwords: set[str] = _get_stopwords("english", "french")

    # Regex that matches all punctuation but the apostrophe (')
    punct_regex: re.Pattern = _get_punct_regex()

    @staticmethod
    def remove_subject(text: str) -> str:
        if text.startswith("Subject: "):
            return text[len("Subject: ") :]
        return text

    @classmethod
    def preprocess(cls, text: str) -> str:

        # Remove "Subject: " from the text
        text = cls.remove_subject(text)

        # Lowercase everything
        text = text.lower()

        text = " ".join(
            w
            for w in text.split()
            if not (
                w in cls.stopwords  # Remove stop words
                or cls.numbers_regex.match(w)  # Remove numbers
                or cls.urls_regex.findall(w)  # Remove URLs
                or cls.email_regex.findall(w)  # Remove email addresses
            )
        )

        # Remove punctuation
        text = cls.punct_regex.sub(" ", text)

        return text
