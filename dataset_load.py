import os

import pandas as pd


class DatasetLoader:
    _csv_files = tuple(
        f + ".csv"
        for f in ("spam_ham_dataset", "completeSpamAssassin", "emails")
    )

    @classmethod
    def get_data(cls, shuffled=True) -> pd.DataFrame:

        # Make sure files exist
        for f in cls._csv_files:
            assert os.access(f, os.R_OK)

        f = iter(cls._csv_files)

        # Dataset 1
        df = pd.read_csv(
            next(f),
            header=0,
            names=("text", "label"),
            usecols=(2, 3),
            na_filter=False,
            skip_blank_lines=True,
        )

        # Dataset 2
        tmp = pd.read_csv(
            next(f),
            header=0,
            names=("text", "label"),
            usecols=(1, 2),
            na_values="empty",
        )
        tmp["text"].replace("^Subject: ", "", inplace=True, regex=True)
        tmp.drop_duplicates(inplace=True)
        df = df.append(tmp, ignore_index=True)

        # Dataset 3
        tmp = pd.read_csv(next(f))
        df = df.append(tmp, ignore_index=True).dropna().drop_duplicates()

        del tmp

        if shuffled:
            df = df.sample(frac=1).reset_index(drop=True)

        return df

    @staticmethod
    def print_stats(df: pd.DataFrame) -> None:
        total = df.shape[0]
        spam = df["label"][df["label"] == 1].count()
        print("Total: {:5d}".format(total))
        print("Spam:  {:5d}".format(spam))
        print("Ham:   {:5d}".format(total - spam))
        print("Percentage of spam: {:.2f}%\n".format(spam / total * 100))
