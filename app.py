#!/usr/bin/env python

import enum
import json
import os
import pathlib
import pickle
import random
import statistics
import time
import datetime

from flask import (
    Flask,
    Markup,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_bootstrap import Bootstrap
from flask_migrate import Migrate
from flask_login import (
    LoginManager,
    UserMixin,
    current_user,
    login_required,
    login_user,
    logout_user,
)
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from sqlalchemy.sql import func as sa_func
from werkzeug.security import check_password_hash, generate_password_hash
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, EqualTo, Length
import joblib
import pandas as pd

import spam_model

APP_DEFINES = {
    "max_name_length": 127,
    "max_email_length": 255,
    "max_subject_length": 255,
    "min_password_length": 5,
    "password_field_size": 102,
    "max_model_path_length": 255,
}

app = Flask(__name__)

app.config["SECRET_KEY"] = b"\x83\xd9\xa0M\x1b\xebD-\x98_\x08\xae;p\x05h"

# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv(
    "DATABASE_URI",
    "mysql+mysqldb://spamuser1:"
    "4T%5BDWO8rqzp(26Z%40@localhost/spamuser1?charset=utf8mb4"
    "&unix_socket=/var/run/mysqld/mysqld.sock",
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

migrate = Migrate(app, db)

app.config["BOOTSTRAP_SERVE_LOCAL"] = True

bootstrap = Bootstrap(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login_route"


class SpamMethod(db.Model):
    __tablename__ = "spam_methods"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(
        db.String(APP_DEFINES["max_name_length"]), nullable=False
    )
    accuracy = db.Column(db.Float, nullable=False)
    precision = db.Column(db.Float, nullable=False)
    recall = db.Column(db.Float, nullable=False)
    f1 = db.Column(db.Float, nullable=False)
    roc_auc = db.Column(db.Float, nullable=False)
    model = db.Column(
        db.String(APP_DEFINES["max_model_path_length"]), nullable=False
    )
    results = db.relationship("SpamResult", back_populates="method")


class User(UserMixin, db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(
        db.String(APP_DEFINES["max_name_length"]), nullable=False
    )
    email = db.Column(
        db.String(APP_DEFINES["max_email_length"]),
        nullable=False,
        unique=True,
    )
    password = db.Column(
        db.String(APP_DEFINES["password_field_size"]), nullable=False
    )
    enabled = db.Column(db.Boolean, nullable=False, default=True)
    sent_mail = db.relationship(
        "Mail", backref="sender", foreign_keys="[Mail.sender_id]"
    )
    received_mail = db.relationship(
        "Mail", backref="recipient", foreign_keys="[Mail.recipient_id]"
    )

    def __repr__(self):
        return f"<User {self.email}>"


class SpamEnum(enum.Enum):
    UNKNOWN = -1
    HAM = 0
    SPAM = 1

    def __str__(self):
        return self.name.capitalize()

    def __repr__(self):
        return f"<SpamEnum {self}>"


class Mail(db.Model):
    __tablename__ = "mails"
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(
        db.DateTime, server_default=sa_func.now(), nullable=False
    )
    subject = db.Column(
        db.String(APP_DEFINES["max_subject_length"]),
        nullable=False,
        default="(no subject)",
    )
    sender_id = db.Column(
        db.Integer, db.ForeignKey(User.__tablename__ + ".id"), nullable=False
    )
    # TODO: create a separate `recepients(user_id, mail_id)`
    # table to support multiple recipients
    recipient_id = db.Column(
        db.Integer, db.ForeignKey(User.__tablename__ + ".id"), nullable=False
    )
    is_spam = db.Column(
        db.Enum(SpamEnum), default=SpamEnum.UNKNOWN, nullable=False
    )
    spam_results = db.relationship("SpamResult", back_populates="mail")
    body = db.Column(db.Text)

    # TODO: Add a column to show if a message is read/unread
    # TODO: add support for file attachements somehow

    def __repr__(self):
        return f"<Mail {self.subject}>"


class SpamResult(db.Model):
    __tablename__ = "spam_results"
    mail_id = db.Column(
        db.Integer, db.ForeignKey("mails.id"), primary_key=True
    )
    method_id = db.Column(
        db.Integer, db.ForeignKey("spam_methods.id"), primary_key=True
    )
    is_spam = db.Column(db.Boolean, nullable=False)
    probability = db.Column(db.Float, nullable=True)
    mail = db.relationship("Mail", back_populates="spam_results")
    method = db.relationship("SpamMethod", back_populates="results")


class LoginForm(FlaskForm):
    email = EmailField(
        "Email address",
        (
            DataRequired(),
            Length(min=3, max=APP_DEFINES["max_email_length"]),
            Email(),
        ),
    )
    password = PasswordField(
        "Password",
        (DataRequired(), Length(min=APP_DEFINES["min_password_length"])),
    )
    remember = BooleanField("Remember me")
    submit = SubmitField("Login")


class SignupForm(FlaskForm):
    name = StringField(
        "Full name",
        (DataRequired(), Length(min=1, max=APP_DEFINES["max_name_length"])),
    )
    email = EmailField(
        "Email address",
        (
            DataRequired(),
            Length(min=3, max=APP_DEFINES["max_email_length"]),
            Email(),
        ),
    )
    password = PasswordField(
        "New Password",
        (DataRequired(), Length(min=APP_DEFINES["min_password_length"])),
    )
    confirm_pw = PasswordField(
        "Confirm password", (DataRequired(), EqualTo("password"))
    )
    submit = SubmitField("Create account")


class SendMailForm(FlaskForm):
    subject = StringField(
        "Subject", (Length(max=APP_DEFINES["max_subject_length"]),)
    )
    recipient = EmailField(
        "Recipient",
        (
            DataRequired(),
            Length(min=3, max=APP_DEFINES["max_email_length"]),
            Email(),
        ),
    )
    body = TextAreaField("Message body", (DataRequired(),))
    submit = SubmitField("Send")


def check_spam(message, mail_id):
    def load_vectorizer():
        return joblib.load("vectorizer")

    df = pd.DataFrame(((message,),), columns=("text",))

    message_vector = load_vectorizer().transform(df["text"])

    predictions = []

    methods = SpamMethod.query.all()

    for method in methods:
        prediction = {"name": method.name, "accuracy": method.accuracy}
        model = pickle.loads(method.model)
        prediction["label"] = model.predict(message_vector)[0]
        predictions.append(prediction)
        result = SpamResult(
            mail_id=mail_id, method_id=method.id, is_spam=prediction["label"]
        )
        db.session.add(result)

    return predictions


def spam_consensus(
    results: list[SpamResult], returnFactors=False
) -> SpamEnum:
    factors = []
    ret = []

    app.logger.debug(f"len(results) = {len(results)}")

    if len(results) == 0:
        if returnFactors:
            return (SpamEnum.HAM, 0, [])
        return 0

    for result in results:
        f1 = 1 if result["is_spam"] else -1
        f2 = (
            result["accuracy"]
            if result["probability"] is None
            else 2 * abs(0.5 - result["probability"])
        )
        f3 = result["f1"]
        factor = f1 * f2 * f3
        factors.append(factor)
        app.logger.debug(
            f"Method {result['method']}: "
            f"{f1} * {f2:.2f} * {f3:.2f} = {factor:.3f}"
        )

        ret.append(
            {
                "method": result["method"],
                "probability": result["probability"],
                "certainty": None
                if result["probability"] is None
                else 2 * abs(0.5 - result["probability"]),
                "f1": result["f1"],
                "accuracy": result["accuracy"],
                "is_spam": result["is_spam"],
                "factor": factor,
            }
        )

    mean = statistics.fmean(factors)
    is_spam = SpamEnum(mean > 0)

    app.logger.debug(f"factors: {factors!r}")
    app.logger.debug(f"mean of factors: {mean}")
    app.logger.debug(f"is_spam: {is_spam}")

    if returnFactors:
        return (is_spam, mean, ret)

    return is_spam


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


# Jinja2 filter for displaying Spam results
@app.template_filter()
def spamresultfilter(value):
    return str(SpamEnum(value))


app.jinja_env.filters["spamresult"] = spamresultfilter


@app.route("/")
def index():
    return render_template("index.html"), {
        "Content-Type": "text/html; charset=utf-8"
    }


@app.route("/login", methods=["GET", "POST"])
def login_route():
    form = LoginForm()

    if form.validate_on_submit():

        user = User.query.filter_by(email=form.email.data).first()

        if not user:
            flash(f"User <{form.email.data}> does not exist!", "error")
            return redirect(url_for("login_route"))

        if not check_password_hash(user.password, form.password.data):
            flash("Wrong password!", "error")
            return redirect(url_for("login_route"))

        login_user(user, remember=form.remember.data)

        flash(f"Logged in successfully as {form.email.data}", "success")

        return redirect(url_for("inbox_route"))

    return render_template("login.html", form=form)


@app.route("/logout")
@login_required
def logout_route():
    logout_user()
    flash("You are now logged out", "success")
    return redirect(url_for("login_route"))


@app.route("/signup", methods=["GET", "POST"])
def signup_route():
    form = SignupForm()
    if form.validate_on_submit():
        pwhash = generate_password_hash(form.password.data)
        new_user = User(
            name=form.name.data, email=form.email.data, password=pwhash
        )
        db.session.add(new_user)
        db.session.commit()
        flash(f"Account <{form.email.data}> created successfully", "success")
        return redirect(url_for("login_route"))
    return render_template("signup.html", form=form)


def check_spam_new(msg: Mail):
    class CheckSpamResponse:
        def __init__(self, **kwargs):
            for k in kwargs.keys():
                setattr(self, k, kwargs[k])

        def __str__(self):
            return str(self.__dict__)

        def __repr__(self):
            return repr(self.__dict__)

        def json(self):
            return json.dumps(self.__dict__) + "\n\n"

    text = "{} {}".format(msg.subject, msg.body)

    method_count, *_ = (
        db.session.query(sa_func.count(SpamMethod.id))
        .filter(SpamMethod.name.notlike("% (old)"))
        .first()
    )

    output = Markup(
        CheckSpamResponse(done=False, methods=method_count).json()
    )
    app.logger.debug(
        f"check_spam_new(): Yielding initial output: {output.strip()})"
    )
    # yield Markup(CheckSpamResponse(done=False, methods=method_count).json())
    yield output

    results_db = []
    results = []

    for i, method in enumerate(
        SpamMethod.query.filter(SpamMethod.name.notlike("% (old)"))
    ):

        resp = CheckSpamResponse(
            done=False, method=method.name, progress=i
        ).json()

        is_spam, *probability = spam_model.predict(method.model, text)
        probability = probability[0] if probability else None

        app.logger.debug(
            f"is_spam: {is_spam!r}, probability: {probability!r}"
        )
        app.logger.debug(f"msg: {msg!r}, method: {method!r}")

        result_db = SpamResult(
            method_id=method.id, is_spam=is_spam, probability=probability
        )

        app.logger.debug(f"result_db: {result_db!r}")

        results_db.append(result_db)

        results.append(
            {
                "method": method.name,
                "is_spam": is_spam,
                "probability": probability,
                "f1": method.f1,
                "accuracy": method.accuracy,
            }
        )

        output = Markup(resp)
        app.logger.debug(
            f"check_spam_new(): Yielding model: {output.strip()})"
        )
        # yield Markup(resp)
        yield output
        time.sleep(0.5)

    msg.is_spam = spam_consensus(results)
    db.session.add(msg)
    db.session.commit()

    app.logger.debug(f"Added message, id={msg.id}")

    for r in results_db:
        r.mail_id = msg.id

    db.session.add_all(results_db)
    db.session.commit()

    app.logger.debug("Added results")

    output = Markup(CheckSpamResponse(done=True, message_id=msg.id).json())
    app.logger.debug(f"check_spam_new(): Yielding id: {output.strip()})")
    yield output
    # yield Markup(CheckSpamResponse(done=True, message_id=msg.id).json())


@app.route("/compose", methods=["GET", "POST"])
@login_required
def compose_route():

    form = SendMailForm()

    if not current_user.enabled:
        app.logger.error(f"Disabled account {current_user}")
        flash("Your account is disabled. You can not send messages.")
        return render_template("compose.html", form=form)

    if not form.validate_on_submit():
        app.logger.error("No validate on submit")
        return render_template("compose.html", form=form)

    recipient = User.query.filter_by(email=form.recipient.data).first()
    if not recipient:
        app.logger.error(f"Recipient not foundin db: {form.recipient.data}")
        flash(f"No such user: <{form.recipient.data}>", "error")
        return render_template("compose.html", form=form)

    app.logger.debug(f"Recipient: {recipient!r}")

    msg = Mail(
        subject=form.subject.data,
        sender_id=current_user.id,
        recipient_id=recipient.id,
        body=form.body.data,
    )

    return app.response_class(check_spam_new(msg))


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + datetime.timedelta(seconds=random_second)


@app.route("/_send", methods=["POST"])
def send_route():
    users = ["bob", "alice", "charlie"]
    start_date = datetime.datetime(1997, 1, 1)
    end_date = datetime.datetime(2020, 12, 31)
    date = random_date(start_date, end_date)

    sender = random.choice(users)
    users.remove(sender)

    receiver = random.choice(users)

    sender = User.query.filter_by(email=sender + "@example.com").first()
    receiver = User.query.filter_by(email=receiver + "@example.com").first()

    msg = Mail(
        subject=request.json["subject"],
        sender_id=sender.id,
        recipient_id=receiver.id,
        body=request.json["body"],
        date=date,
    )

    app.logger.debug("--- SENDING")
    app.logger.debug(
        "From {} ({}), To {} ({}), date: {}".format(
            sender.email, sender.id, receiver.email, receiver.id, date
        )
    )
    app.logger.debug("Subject: {}".format(msg.subject))
    app.logger.debug("---")

    resp = app.response_class(check_spam_new(msg))
    resp.headers.set("Content-Type", "text/plain; charset=utf-8")

    app.logger.debug("---")

    return resp


@app.route("/compose-old", methods=["GET", "POST"])
@login_required
def compose_route_old():
    form = SendMailForm()
    if form.validate_on_submit():
        recipient = User.query.filter_by(email=form.recipient.data).first()
        if not recipient:
            flash(f"No such user: <{form.recipient.data}>", "error")
            return render_template("compose-old.html", form=form)

        msg = Mail(
            subject=form.subject.data,
            sender=current_user,
            recipient=recipient,
            body=form.body.data,
            is_spam=SpamEnum.UNKNOWN,
        )

        db.session.add(msg)
        db.session.commit()

        predictions = check_spam(
            f"{form.subject.data}\n{form.body.data}", msg.id
        )

        # TODO: Refactor this into a function
        spam_meter = 0
        spam_total = 0
        for prediction in predictions:
            spam_meter += prediction["label"] * prediction["accuracy"]
            spam_total += prediction["accuracy"]
        spam_meter /= spam_total

        is_spam = SpamEnum.HAM
        if spam_meter >= 0.5:
            is_spam = SpamEnum.SPAM

        msg.is_spam = is_spam

        db.session.commit()

        flash(f"Mail <{form.subject.data}> sent successfully", "success")

        return redirect(url_for("outbox_route"))

    return render_template("compose-old.html", form=form)


@app.route("/inbox")
@login_required
def inbox_route():
    # TODO: implement pagination somehow, you can't show all the messages
    # if there are thousands of them.
    messages = (
        Mail.query.filter_by(recipient=current_user)
        .order_by(Mail.date.desc())
        .all()
    )
    app.logger.debug(f"Got {len(messages)} messages")
    for i, message in enumerate(messages):
        app.logger.debug(f"Message {i}: {message!s}")
    return render_template("inbox.html", name="Inbox", messages=messages)


@app.route("/outbox")
@login_required
def outbox_route():
    messages = (
        Mail.query.filter_by(sender=current_user)
        .order_by(Mail.date.desc())
        .all()
    )
    return render_template("inbox.html", name="Outbox", messages=messages)


@app.route("/message/<int:message_id>")
@login_required
def message_route(message_id):
    msg = Mail.query.get(message_id)

    if not msg:
        flash("Message not found!", "error")
        return redirect(url_for("inbox_route"))

    if (
        msg.sender_id != current_user.id
        and msg.recipient_id != current_user.id
    ):
        flash("You are not authorized to view that message!", "error")
        return redirect(url_for("inbox_route"))

    spam_results = msg.spam_results

    show_spam = msg.sender_id != current_user.id

    results = []
    score = 0

    if show_spam:

        for r in spam_results:
            method = r.method
            results.append(
                {
                    "method": method.name,
                    "is_spam": r.is_spam,
                    "probability": r.probability,
                    "f1": method.f1,
                    "accuracy": method.accuracy,
                }
            )
        is_spam, score, results = spam_consensus(results, True)

    return render_template(
        "message.html",
        message=msg,
        show_spam=show_spam,
        results=results,
        consensus=score,
    )


@app.route("/favicon.ico")
@app.route("/favicon.png")
def favicon_route():
    return app.send_static_file("images/favicon.png")


@app.route("/stream", methods=["GET", "POST"])
def streamroute():
    def f():
        yield "Start\n"
        for i in range(20):
            yield f"{i} seconds elapsed\n"
            time.sleep(1)
        yield "Done\n"

    return app.response_class(f())


@app.route("/stream2")
def streamroute2():
    return render_template("stream.html")


def _spam_check(text):
    predictions = []
    for method in SpamMethod.query.all():
        model_path = pathlib.Path(method.model)
        is_spam = spam_model.predict(model_path, text)
        prediction = {"name": method.name, "is_spam": is_spam[0]}
        if len(is_spam) > 1:
            prediction["probability"] = is_spam[1]
        predictions.append(prediction)
    return predictions


@app.route("/spam-check", methods=["GET", "POST"])
def spam_check_route():
    if request.method == "POST":
        predictions = _spam_check(request.form["text"])
        return jsonify({"predictions": predictions})
    return (
        '<!doctype html><html lang="en"><meta charset="utf-8">'
        '<title>Spam check</title><form method="post">'
        '<label for="t">Message text</label>'
        '<textarea id="t" name="text" width="80" height="24"></textarea>'
        '<button type="submit">Check message</button></form>'
    )


@app.shell_context_processor
def make_shell_context():
    return dict(
        db=db,
        User=User,
        Mail=Mail,
        SpamMethod=SpamMethod,
        SpamResult=SpamResult,
    )


@app.before_first_request
def load_models():
    try:
        for method in SpamMethod.query.filter(
            SpamMethod.name.notlike("% (old)")
        ):
            spam_model.SpamModel.load_model(method.model)
            app.logger.debug(f"Loaded model: {method.name} ({method.model})")
    except Exception as e:
        app.logger.error(f"Failed to load models: {str(e)}")


if __name__ == "__main__":
    app.run(debug=True)
