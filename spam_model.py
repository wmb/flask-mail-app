import enum
import pathlib
import pickle
import typing as t
import zlib

from sklearn.base import BaseEstimator
from sklearn.feature_extraction.text import TfidfVectorizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import joblib
import numpy as np
import tensorflow as tf

from text_preprocess import PreprocessText


class ModelType(enum.Enum):
    ML = 0
    DL = 1


ModelObj = t.Union[BaseEstimator, pathlib.Path, str]


class SpamModel:

    # Caches to avoid having to read files multiple times
    instances = {}
    tokenizers = {}
    models = {}

    def __init__(
        self,
        name: str,
        model: ModelObj,
        model_type: ModelType,
        tokenizer_path: pathlib.Path,
        tokenizer_info: dict = None,
    ):
        self.name = name
        self.model = model
        self.type = model_type
        self.tokenizer = tokenizer_path
        self.tokenizer_info = tokenizer_info

    def pickled(self) -> bytes:
        p = pickle.dumps(self, protocol=5, fix_imports=False)
        return zlib.compress(p, zlib.Z_BEST_COMPRESSION)

    def predict(self, text) -> t.Union[tuple[bool, float], tuple[bool]]:
        text = PreprocessText.preprocess(text)
        a = np.reshape(np.asarray(text, dtype=np.str_), (1,))

        tokkey = str(self.tokenizer)
        t = SpamModel.tokenizers.get(tokkey)

        if t is None:
            t = joblib.load(self.tokenizer)
            SpamModel.tokenizers[tokkey] = t

        modkey = hash(self.model)
        model = SpamModel.models.get(modkey)

        if self.type == ModelType.ML:  # Machine learning models
            m = t.transform(a)
            pred = self.model.predict(m)[0]
            return (bool(pred),)

        elif self.type == ModelType.DL:  # Deep learning models
            # model = tf.keras.models.load_model(pathlib.Path(self.model))
            maxlen = self.tokenizer_info.get("maxlen")
            m = pad_sequences(t.texts_to_sequences(a), maxlen)
            pred = model(m)
            pred = float(np.reshape(pred.numpy(), (1,))[0])
            return (bool(pred >= 0.5), pred)

        return (False,)

    @staticmethod
    def unpickle(pickled: pathlib.Path):  # -> SpamModel
        key = str(pickled)
        if key in SpamModel.instances:
            return SpamModel.instances[key]
        p = zlib.decompress(pickled.read_bytes())
        SpamModel.instances[key] = pickle.loads(
            p, fix_imports=False, encoding="bytes"
        )
        return SpamModel.instances[key]

    @staticmethod
    def load_model(key: str):
        m = SpamModel.instances.get(key)
        if m is None:
            m = SpamModel.unpickle(pathlib.Path(key))
        t = SpamModel.tokenizers.get(str(m.tokenizer))
        if t is None:
            t = joblib.load(m.tokenizer)
            SpamModel.tokenizers[str(m.tokenizer)] = t
        model = None
        key = hash(m.model)
        if key not in SpamModel.models:
            if m.type == ModelType.ML:
                model = m.model
            elif m.type == ModelType.DL:
                model = tf.keras.models.load_model(pathlib.Path(m.model))
            SpamModel.models[key] = model


def predict(
    model_path: t.Union[pathlib.Path, str], text: str
) -> t.Union[tuple[bool, float], tuple[bool]]:
    return SpamModel.unpickle(pathlib.Path(model_path)).predict(text)
